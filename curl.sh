#!/bin/sh
set -x
curl -s --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" "$1"
